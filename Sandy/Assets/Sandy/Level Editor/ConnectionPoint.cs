using System;
using UnityEngine;

public enum ConnectionPointType { In, Out }

public class ConnectionPoint
{
	#region Parameters
		public Rect rect;

		public ConnectionPointType type;

		public Scene scene;

		public GUIStyle style;

		public Action<ConnectionPoint> OnClickConnectionPoint;

		public ConnectionPoint(Scene scene, ConnectionPointType type, GUIStyle style, Action<ConnectionPoint> OnClickConnectionPoint)
		{
			this.scene = scene;
			this.type = type;
			this.style = style;
			this.OnClickConnectionPoint = OnClickConnectionPoint;
			rect = new Rect(0, 0, 10f, 20f);
		}
		public ConnectionPoint(){}
	#endregion

	#region GUI Methods
		public void Draw()
		{
			rect.y = scene.rect.y + (scene.rect.height * 0.5f) - rect.height * 0.5f;
	
			switch (type)
			{
				case ConnectionPointType.In:
					rect.x = scene.rect.x - rect.width + 3f;
					break;
	
				case ConnectionPointType.Out:
					rect.x = scene.rect.x + scene.rect.width - 3f;
					break;
			}
	
			if (GUI.Button(rect, "", style))
				if (OnClickConnectionPoint != null)
					OnClickConnectionPoint(this);
		}
	#endregion
}