﻿using System;
using UnityEditor;
using UnityEngine;
using Sandy;
using System.Collections.Generic;

public class LevelEditor : DesignMaker
{
	#region Parameters
		[SerializeField] public static int instanceCreated = 0; //How many node we have 
		[SerializeField] private List<Scene> scenes; //List of all the scenes
		[SerializeField] private List<Connection> connections = null; // List of all the connection nodes

		//Styling
		[SerializeField] private GUIStyle _inPointStyle;
		[SerializeField] private GUIStyle _outPointStyle;

		//Connection Points (in and out)
		[SerializeField] private ConnectionPoint _selectedInPoint;
		[SerializeField] private ConnectionPoint _selectedOutPoint;

		[SerializeField] private Vector2 _offset; //Grid
		[SerializeField] private Vector2 _drag; //Position Grid

		public static new void OpenWindow()
		{
			LevelEditor window = GetWindow<LevelEditor> ();
			window.titleContent = new GUIContent ("Sandy Editor");
			window.position = new Rect(15, 15 , 1000, 1000);
		}

		private void OnEnable()
		{
			_inPointStyle = new GUIStyle ();
			_inPointStyle.normal.background = EditorGUIUtility.Load ("builtin skins/darkskin/images/btn left.png") as Texture2D;
			_inPointStyle.active.background = EditorGUIUtility.Load ("builtin skins/darkskin/images/btn left on.png") as Texture2D;
			_inPointStyle.border = new RectOffset (4, 4, 12, 12);

			_outPointStyle = new GUIStyle ();
			_outPointStyle.normal.background = EditorGUIUtility.Load ("builtin skins/darkskin/images/btn right.png") as Texture2D;
			_outPointStyle.active.background = EditorGUIUtility.Load ("builtin skins/darkskin/images/btn right on.png") as Texture2D;
			_outPointStyle.border = new RectOffset (4, 4, 12, 12);
		}
	#endregion

	#region GUI Methods
		private void OnGUI()
		{
			DrawGrid (20, 0.2f, Color.gray); //Grid in the Grid
			DrawGrid (100, 0.4f, Color.gray); //Level Editor Grid

			DrawScenes ();
			DrawConnections ();

			DrawConnectionLine (Event.current);

			ProcessNodeEvents (Event.current);
			ProcessEvents (Event.current);

			//if(GUI.changed) Repaint();

		}

		private void DrawGrid (float gridSpacing, float gridOpacity, Color gridColor)
		{
			int widthDivs = Mathf.CeilToInt (position.width / gridSpacing);
			int heightDivs = Mathf.CeilToInt (position.height / gridSpacing);

			Handles.BeginGUI ();
			Handles.color = new Color (gridColor.r, gridColor.g, gridColor.b, gridOpacity);

			_offset += _drag * 0.5f;
			Vector3 newOffset = new Vector3 (_offset.x % gridSpacing, _offset.y % gridSpacing, 0);

			for (int i = 0; i < widthDivs; i++)
				Handles.DrawLine (new Vector3 (gridSpacing * i, -gridSpacing, 0) + newOffset, new Vector3 (gridSpacing * i, position.height, 0f) + newOffset);

			for (int j = 0; j < heightDivs; j++)
				Handles.DrawLine (new Vector3 (-gridSpacing, gridSpacing * j, 0) + newOffset, new Vector3 (position.width, gridSpacing * j, 0f) + newOffset);

			Handles.color = Color.white;
			Handles.EndGUI ();
		}

		//Draw nodes
		private void DrawScenes ()
		{
			if (scenes != null)
				for (int i = 0; i < scenes.Count; i++)
				{
					scenes[i].DrawBox ();
					scenes[i].DrawHeader ();
					scenes[i].DrawContent ();
				}
		}

		private void DrawConnections ()
		{
			if (connections != null)
				for (int i = 0; i < connections.Count; i++)
					connections[i].Draw ();
		}

		private void ProcessEvents (Event e)
		{
			_drag = Vector2.zero;

			switch (e.type) 
			{
				case EventType.MouseDown:
					if (e.button == 0) ClearConnectionSelection ();

					if (e.button == 1) ProcessContextMenu (e.mousePosition);
					break;

				//Making the node draggable
				case EventType.MouseDrag:
					if (e.button == 0) OnDrag (e.delta);
					break;
			}
		}

		private void ProcessNodeEvents (Event e)
		{
			if (scenes != null)
				for (int i = scenes.Count - 1; i >= 0; i--)
				{
					bool guiChanged = scenes[i].ProcessEvents (e);

					if (guiChanged)
						GUI.changed = true;
				}
		}

		//Drawing the bezier from selected connection point to the mouse position. By drawing this bezier
		//I will let users know which connection point selected and how their connection will look like
		private void DrawConnectionLine (Event e)
		{
			//Output can't connect with an output
			//Input can't connect with an input
			//Can't draw a connection line if you're connecting the input and the output of the same node

			if (_selectedInPoint != null && _selectedOutPoint == null)
			{
				Handles.DrawBezier (
					_selectedInPoint.rect.center,
					e.mousePosition,
					_selectedInPoint.rect.center + Vector2.left * 50f,
					e.mousePosition - Vector2.left * 50f,
					Color.green,
					null,
					4f
				);

				GUI.changed = true;
			}

			if (_selectedOutPoint != null && _selectedInPoint == null)
			{
				Handles.DrawBezier (
					_selectedOutPoint.rect.center,
					e.mousePosition,
					_selectedOutPoint.rect.center - Vector2.left * 50f,
					e.mousePosition + Vector2.left * 50f,
					Color.green,
					null,
					4f
				);

				GUI.changed = true;
			}
		}
		private void ProcessContextMenu (Vector2 mousePosition)
		{
			GenericMenu genericMenu = new GenericMenu ();

			Debug.Log(instanceCreated);

			if (instanceCreated < 0)
				instanceCreated = 0;

			if (instanceCreated == 0)
			{
				genericMenu.AddItem (new GUIContent ("Add First Scene"), false, () => OnClickAddFirstScene (mousePosition));
				genericMenu.AddItem (new GUIContent ("Save"), false, () => SaveCurrent());
				genericMenu.AddItem (new GUIContent ("Load"), false, () => LoadEditor());
				genericMenu.ShowAsContext ();
			}
			else
			{
				if (instanceCreated == 1)
				{
					genericMenu.AddItem (new GUIContent ("Add Connector"), false, () => OnClickAddConnectorNode (mousePosition));
					genericMenu.AddItem (new GUIContent ("Save"), false, () => SaveCurrent());
					genericMenu.AddItem (new GUIContent ("Load"), false, () => LoadEditor());
					genericMenu.ShowAsContext ();
				}
				else
				{
					if (instanceCreated == 2)
					{
						genericMenu.AddItem (new GUIContent ("Add Next Scene"), false, () => OnClickAddNextScene (mousePosition));
						genericMenu.AddItem (new GUIContent ("Save"), false, () => SaveCurrent());
						genericMenu.AddItem (new GUIContent ("Load"), false, () => LoadEditor());
						genericMenu.ShowAsContext ();
					}
					else
					{
						genericMenu.AddItem (new GUIContent ("Add Next Scene"), false, () => OnClickAddNextScene (mousePosition));
						genericMenu.AddItem (new GUIContent ("Add Connector"), false, () => OnClickAddConnectorNode (mousePosition));
						genericMenu.AddItem (new GUIContent ("Save"), false, () => SaveCurrent());
						genericMenu.AddItem (new GUIContent ("Load"), false, () => LoadEditor());
						genericMenu.ShowAsContext ();
					}
				}
			}
		}

		private void OnDrag (Vector2 delta)
		{
			_drag = delta;

			if (scenes != null)
				for (int i = 0; i < scenes.Count; i++)
					scenes[i].Drag (delta);

			GUI.changed = true;
		}

		public void SaveCurrent()
		{
			// Data editorData = new Data()
			// {
			// 	dateTime = DateTime.Now,
			// 	instanceCreated = instanceCreated,
			// 	scenes = scenes,
			// 	connections = connections
			// };
			//SandyConfigurator.SandyData(3, Json.Write, editorData);
			SandyConfigurator.JsonLevelEditor(Json.Write, this);
		}

		public void LoadEditor()
		{
			//SandyConfigurator.SandyData(3, Json.Read, null);

			// instanceCreated = SandyConfigurator.dataLoaded.instanceCreated;
			// scenes = SandyConfigurator.dataLoaded.scenes;
			// connections = SandyConfigurator.dataLoaded.connections;

			// Repaint();
			SandyConfigurator.JsonLevelEditor(Json.Read, this);
		}
	#endregion

	#region Helpers
		private int SceneId(List<Scene> scenes)
		{
			int id = scenes.Count;
			
			if(scenes == null)
				return 0;

			foreach (Scene scene in scenes)
				if (id == scene.sceneId)
					id++;
			return id;
		}

		private void OnClickAddFirstScene (Vector2 mousePosition)
		{
			if (scenes == null)
				scenes = new List<Scene> ();

			scenes.Add (new FirstScene (SceneId(scenes), mousePosition, SceneId(scenes).ToString (), 400, 270, _inPointStyle, _outPointStyle, null, OnClickOutPoint, OnClickRemoveScene));
			instanceCreated = SceneId(scenes);
		}

		private void OnClickAddNextScene (Vector2 mousePosition)
		{
			if (scenes == null)
				scenes = new List<Scene> ();

			scenes.Add (new NextScene (SceneId(scenes), mousePosition, SceneId(scenes).ToString (), 400, 270, _inPointStyle, _outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveScene));
			instanceCreated = SceneId(scenes);
		}

		private void OnClickAddConnectorNode (Vector2 mousePosition)
		{
			if (scenes == null)
				scenes = new List<Scene> ();

			scenes.Add (new ConnectorNode (SceneId(scenes), mousePosition, SceneId(scenes).ToString (), 300, 200, _inPointStyle, _outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveScene));
			instanceCreated = SceneId(scenes);
		}

		//Start Drawing from an in point
		private void OnClickInPoint (ConnectionPoint inPoint)
		{
			_selectedInPoint = inPoint;
			if (_selectedOutPoint != null)
			{
				if (_selectedOutPoint.scene != _selectedInPoint.scene)
				{
					CreateConnection ();
					ClearConnectionSelection ();
				}
				else
					ClearConnectionSelection ();
			}
		}

		//Start Drawing from an out point
		private void OnClickOutPoint (ConnectionPoint outPoint)
		{
			_selectedOutPoint = outPoint;
			if (_selectedInPoint != null)
			{
				if (_selectedOutPoint.scene != _selectedInPoint.scene)
				{
					CreateConnection ();
					ClearConnectionSelection ();
				}
				else
					ClearConnectionSelection ();
			}
		}

		//Delele Scene
		private void OnClickRemoveScene (Scene scene)
		{
			if (connections != null || scene.connectedWith.Count > 0)
			{
				List<Connection> connectionsToRemove = new List<Connection> ();

				foreach (Connection c in connections)
					if (c.to == scene.to || c.to == scene.from || c.from == scene.from)
						connectionsToRemove.Add (c);

				foreach (Connection c in connectionsToRemove)
					connections.Remove (c);
				
				connectionsToRemove = null;
			}
			scenes.Remove (scene);
			instanceCreated--;
		}

		//Remove the connection if clicked in the middle square
		private void OnClickRemoveConnection (Connection connection)
		{
			connections.Remove (connection);
		}
		//Create the connection between 2 scenes
		private void CreateConnection ()
		{
			if (connections == null)
				connections = new List<Connection> ();
			
			Connection connection = new Connection (_selectedInPoint, _selectedOutPoint, OnClickRemoveConnection);

			Debug.LogWarning(connection.from.scene.sceneId+ " is connecting with " + connection.to.scene.sceneId + " and it's a " + UniqueConnection(connection) + " unique connection");
			Debug.Log(connection.from.scene + " BASE Node type is : " + TypeOfNodeFiniteStateMachine(connection.from.scene));
			Debug.Log(connection.to.scene + " CHILD Node type is : " + TypeOfNodeFiniteStateMachine(connection.to.scene));
			
			if (ValidConnectionRules (connection))
			{
				connections.Add (connection);
				connection.from.scene.connectedWith.Add (connection.to.scene);
				connection.to.scene.recieveConnection = connection.from.scene;
				CreateName(connection);
			}
		}

		//Test if the connection is Unique And connecting to a node connector
		private bool ValidConnectionRules (Connection connection)
		{
			return (UniqueConnection(connection) && DifferentConnectionType(connection) && ConnectorRecievingOneConnectionTo(connection) && NumberOfConnectionsAllowed(connection));
		}

		private bool ConnectorRecievingOneConnectionTo(Connection connection)
		{
			if(TypeOfNodeFiniteStateMachine(connection.to.scene) == TypeOfNode.Connector && connection.to.scene.recieveConnection != null)
			{
				Debug.LogError("Connector must have only one parent");
				ClearConnectionSelection ();
				return false;
			}
			else
				return true;
			
		}

		private bool UniqueConnection(Connection connection)
		{
			foreach (Connection c in connections)
			{
				//1st part of condition : prevent connecting one than once with the same node
				//2nd part of condition : allow different sources to connect the same node
				if (c.to == connection.to && c.from.scene.sceneId == connection.from.scene.sceneId)
				{
					ClearConnectionSelection ();
					return false;
				}
			}
			return true;
		}

		private bool DifferentConnectionType(Connection connection)
		{
			if (TypeOfNodeFiniteStateMachine(connection.to.scene) == TypeOfNodeFiniteStateMachine(connection.from.scene))
			{
				Debug.LogError("Not allowed Same type of nodes, must be scene + connector");
				ClearConnectionSelection ();
				return false;
			}
			else
				return true;
		}

		private bool NumberOfConnectionsAllowed(Connection connection)
		{
			if (connection.from.scene.connectedWith.Count >= 10)
			{
				Debug.LogError("Maximum of 9 connections is allowed");
				ClearConnectionSelection ();
				return false;
			}
			else
				return true;
		}

		private TypeOfNode TypeOfNodeFiniteStateMachine (Scene scene)
		{
			string sceneType = scene.GetType ().UnderlyingSystemType.Name;
			return (sceneType.Equals("ConnectorNode") ? TypeOfNode.Connector : TypeOfNode.Scene);
		}
		protected enum TypeOfNode
		{
			Scene = 0, Connector = 1
		}

		//Resets Drawing connection
		private void ClearConnectionSelection ()
		{
			_selectedInPoint = null;
			_selectedOutPoint = null;
		}

		private void CreateName(Connection connection)
		{
			if (TypeOfNodeFiniteStateMachine(connection.from.scene) == TypeOfNode.Scene)
				if (connection.from.scene.sceneName == "")
				{
					connection.from.scene.sceneName = "Level " + connection.from.scene.sceneId;
					connection.from.scene.title = "Level " + connection.from.scene.sceneId;
					connection.from.scene.showEdit = true;
					connection.from.scene.SaveScene();
				}
		}
	#endregion
}
