﻿using System;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

[JsonObject(IsReference = true)]
[Serializable]
public abstract class Scene {
	/*
	A scene is basically a node but since this is a scene editor, it should contain a list of scenes. A scene will be responsible for drawing itself and processing its own events. Unlike ProcessEvents(Event e) in Level Editor, ProcessEvents(Event e) in Scene will return a boolean so that we can check whether we should repaint the GUI or not.
	*/

	#region Parameters
		public int sceneId = 0; //Id of the scene
		public Rect rect = new Rect(0,0,0,0); //W and H of the node
		public string title = ""; //Title of the scene
		public bool isDragged = false; //Check if is dragging
		public bool isSelected = false; //Check if selected

		public ConnectionPoint to = null; //Connection Point in
		public ConnectionPoint from = null; //Connection Point out

		public GUIStyle style = null; //Texture of the node
		public GUIStyle defaultNodeStyle = null; //Deflault Texture of the node
		public GUIStyle selectedNodeStyle = null; //When selecting a node > change texture

		public Action<Scene> OnRemoveNode = null; //Action on remove scene

		//Scene objects and modifiers
		public UnityEngine.Object gameObject = null;
		public string sceneName = "";
		public const string ScenePath = "Assets/Sandy Scenes/";
		public enum GameObjectTypes
		{
			empty = 0, player = 1, boss = 2, enemy = 3, npc = 4, wall = 5
		}

		public struct SandyGameObjects
		{
			public int id;
			public UnityEngine.SceneManagement.Scene scene;
			public string objectName;
			public GameObjectTypes gameObjectType;
			public SandyGameObjects (int id, UnityEngine.SceneManagement.Scene scene, string objectName, GameObjectTypes gameObjectType)
			{
				this.id = id;
				this.scene = scene;
				this.objectName = objectName;
				this.gameObjectType = gameObjectType;
			}
		}
		public LinkedList<SandyGameObjects> sandyGameObjects = new LinkedList<SandyGameObjects> ();

		public List<Scene> connectedWith = new List<Scene> ();
		public Scene recieveConnection = null;

		public bool showEdit = false;

		//Scene box constructor
		public Scene (int id, Vector2 position, string title, float width, float height, GUIStyle inPointStyle, GUIStyle outPointStyle, Action<ConnectionPoint> 	OnClickInPoint, Action<ConnectionPoint> OnClickOutPoint, Action<Scene> OnClickRemoveNode)
		{
			sceneId = id; //id of the scene for direct acces purposes
			rect = new Rect (position.x, position.y, width, height); //Position of the Scene
			this.title = title; //Scene Title
			to = new ConnectionPoint (this, ConnectionPointType.In, inPointStyle, OnClickInPoint); //Connection Point in
			from = new ConnectionPoint (this, ConnectionPointType.Out, outPointStyle, OnClickOutPoint); //Connection Point out
			OnRemoveNode = OnClickRemoveNode; //Action on remove Scene
		}

		//Game Objects Contructor
		public Scene ()
		{
			//WIP: TODO Saving scene
		}
	#endregion

	#region GUI Methods
		//Drag Scene
		public virtual void Drag (Vector2 delta)
		{
			rect.position += delta;
		}

		//Draw Scene box with connection points, Header (title) and scene content
		public virtual void DrawBox ()
		{
			to.Draw ();
			from.Draw ();
		}

		public virtual void DrawHeader ()
		{
			string boxTitle = title + " [" + sceneId.ToString () + "]";
			GUI.Box (rect, "");

			GUILayout.BeginArea (rect);
			EditorGUILayout.BeginHorizontal ();
			GUI.Label (new Rect ((rect.width) / 2 - (rect.width) / 8, 0, rect.width, rect.height), boxTitle, EditorStyles.boldLabel);
			EditorGUILayout.EndHorizontal ();
			GUILayout.EndArea ();
		}

		public virtual void DrawContent ()
		{

		}

		public bool ProcessEvents (Event e)
		{
			switch (e.type)
			{
				case EventType.MouseDown:
					if (e.button == 0)
					{
						if (rect.Contains (e.mousePosition))
						{
							isDragged = true;
							GUI.changed = true;
							isSelected = true;
							style = selectedNodeStyle;
							if (EditorSceneManager.GetSceneByName (sceneName).IsValid () && EditorSceneManager.GetSceneByName (sceneName).isLoaded)
								EditorSceneManager.OpenScene (ScenePath + sceneName + ".unity", OpenSceneMode.Single);
						}
						else
						{
							GUI.changed = true;
							isSelected = false;
							style = defaultNodeStyle;
						}
					}

					if (e.button == 1 && isSelected && rect.Contains (e.mousePosition))
					{
						ProcessContextMenu ();
						e.Use ();
					}

					break;

				case EventType.MouseUp:
					isDragged = false;
					break;

				case EventType.MouseDrag:
					if (e.button == 0 && isDragged)
					{
						Drag (e.delta);
						e.Use ();
						return true;
					}
					break;
			}

			return false;
		}
	#endregion

	#region helpers
		private void ProcessContextMenu ()
		{
			GenericMenu genericMenu = new GenericMenu ();
			genericMenu.AddItem (new GUIContent ("Delete"), false, OnClickRemoveNode);
			genericMenu.ShowAsContext ();
		}

		public void OnClickRemoveNode ()
		{
			//Delete the scene and all its child scenes.
			if (OnRemoveNode != null)
			{
				RemoveScene (true);
				OnRemoveNode (this);

				LevelEditor.instanceCreated--;

				//This make the method works recursively.
				foreach (var scene in connectedWith)
					scene.OnClickRemoveNode();
			}
		}

		public bool SceneIsActive (string sceneName)
		{
			return (EditorSceneManager.GetSceneByName (sceneName).IsValid () && EditorSceneManager.GetSceneByName (sceneName).isLoaded);
		}

		public void CreateGameObject (GameObjectTypes op, string name, Scene scene)
		{
			if (scene.SceneIsActive (sceneName) && name.Length >= 1)
			{
				switch (op)
				{
					case GameObjectTypes.empty:
						GameObject empty = new GameObject();
						empty.transform.position = Vector3.zero;
						empty.transform.name = name;
						break;
					case GameObjectTypes.player:
						GameObject player = GameObject.CreatePrimitive (PrimitiveType.Sphere);
						player.tag = "Player";
						player.transform.position = Vector3.zero;
						player.transform.name = name;
						player.AddComponent<BoxCollider2D>();
						break;
					case GameObjectTypes.boss:
						GameObject boss = GameObject.CreatePrimitive (PrimitiveType.Plane);
						boss.tag = "Enemy";
						boss.transform.position = Vector3.zero;
						boss.transform.name = name;
						boss.AddComponent<BoxCollider2D>();
						break;
					case GameObjectTypes.enemy:
						GameObject enemy = GameObject.CreatePrimitive (PrimitiveType.Plane);
						enemy.tag = "Enemy";
						enemy.transform.position = Vector3.zero;
						enemy.transform.name = name;
						enemy.AddComponent<BoxCollider2D>();
						break;
					case GameObjectTypes.npc:
						GameObject npc = GameObject.CreatePrimitive (PrimitiveType.Plane);
						npc.transform.position = Vector3.zero;
						npc.transform.name = name;
						npc.AddComponent<BoxCollider2D>();
						break;
					case GameObjectTypes.wall:
						GameObject wall = GameObject.CreatePrimitive (PrimitiveType.Plane);
						wall.transform.position = Vector3.zero;
						wall.transform.name = name;
						wall.AddComponent<BoxCollider2D>();
						break;
					default:
						Debug.LogError ("Unrecognized Option in the GDD");
						break;
				}
			}
		}

		public void SaveScene ()
		{
			//This will save all the scene infos
			if (title.Length >= 1)
			{
				if (!SceneIsActive (sceneName))
				{
					var newScene = EditorSceneManager.NewScene (NewSceneSetup.EmptyScene, NewSceneMode.Single);
					EditorSceneManager.SaveScene (newScene, ScenePath + sceneName + ".unity");
				}
				else
				{
					var oldScene = EditorSceneManager.GetSceneByName (sceneName);
					EditorSceneManager.SaveScene (oldScene, ScenePath + sceneName + ".unity");
				}
			}
		}

		public void RemoveScene (bool confirm)
		{
			//WIP
			var removeableScene = EditorSceneManager.GetSceneByName (sceneName);
			EditorSceneManager.OpenScene ("Assets/Sandy/Configuration/Do not delete.unity", OpenSceneMode.Single);
			EditorSceneManager.CloseScene (removeableScene, confirm);
			File.Delete (path: "../Sandy Scenes" + sceneName + ".unity");
		}
	#endregion
}