using System;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using Newtonsoft.Json;
using System.Collections.Generic;

[JsonObject(IsReference = true)]
[Serializable]
public class FirstScene : Scene {
	#region Parameters
		private string objectName = "";
		private GameObjectTypes _gameObjectType = new GameObjectTypes();
		private int _element = 0;

		public FirstScene (int id, Vector2 position, string title, float width, float height, GUIStyle inPointStyle, GUIStyle outPointStyle, 	Action<ConnectionPoint> OnClickInPoint, Action<ConnectionPoint> OnClickOutPoint, Action<Scene> OnClickRemoveNode) : base (id, position, title, width, height, inPointStyle, outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveNode)
		{
			//First Scene Contructor
		}
	#endregion

	#region GUI Methods
		public override void Drag (Vector2 delta)
		{
			base.Drag (delta);
		}

		//Draw :
		// 1) Scene box with connection points
		// 2) Header (title)
		// 3) Scene content

		public override void DrawBox ()
		{
			base.DrawBox ();
		}

		public override void DrawHeader ()
		{
			base.DrawHeader ();
		}

		public override void DrawContent ()
		{
			base.DrawContent ();

			GUILayout.BeginArea (rect);

				GUILayout.Space (30);

				EditorGUI.BeginDisabledGroup (showEdit);

				//Get and Change the Scene name
					EditorGUILayout.BeginHorizontal ();

						GUILayout.Label ("Scene Name");
						sceneName = EditorGUILayout.TextField (title);
						if (sceneName != title || Int32.TryParse (sceneName, out int temp).Equals (false))
							title = sceneName;
						else
							title = "";

					EditorGUILayout.EndHorizontal ();

				EditorGUI.EndDisabledGroup ();

				GUILayout.Space (3);

				EditorGUILayout.BeginHorizontal ();
					EditorGUILayout.HelpBox ("Warning ! By clicking on edit.\nYou no longer can re-name your scene", MessageType.Error);
				EditorGUILayout.EndHorizontal ();

				GUILayout.Space (3);

				EditorGUILayout.BeginHorizontal ();
					if (GUILayout.Button ("Edit Scene", GUILayout.Height (15)) && title.Length >= 1)
						showEdit = true;
				EditorGUILayout.EndHorizontal ();

				GUILayout.Space (3);

				if (showEdit)
				{
					//Game Obeject Manipulation
					EditorGUILayout.BeginHorizontal ();

					GUILayout.Label ("Test on object");
					gameObject = EditorGUILayout.ObjectField (gameObject, typeof (UnityEngine.Object), true);

					EditorGUILayout.EndHorizontal ();

					GUILayout.Space (3);

					//Object Creator
					this.AddGameObject ();

					GUILayout.Space (3);

				}

				EditorGUILayout.BeginHorizontal ();
					if (GUILayout.Button ("Save Scene", GUILayout.Height (15)))
						SaveScene ();
				EditorGUILayout.EndHorizontal ();

			GUILayout.EndArea ();
		}
	#endregion

	#region Helpers
		public void AddGameObject ()
		{
			EditorGUILayout.BeginHorizontal ();
			GUILayout.Label ("Create object");
			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.BeginHorizontal ();
			GUILayout.Label ("Name");
			//temporarlly saving to avoid conflicts
			this.objectName = EditorGUILayout.TextField (this.objectName);
			this._gameObjectType = (GameObjectTypes) EditorGUILayout.EnumPopup (this._gameObjectType);

			//Adding to a list for saving
			base.sandyGameObjects.AddLast (new SandyGameObjects (_element, UnityEngine.SceneManagement.SceneManager.GetSceneByName (sceneName), this.objectName, 	this._gameObjectType));
			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.BeginHorizontal ();
			if (GUILayout.Button ("Create object", GUILayout.Height (15)) && this.objectName != null)
			{
				CreateGameObject (this._gameObjectType, this.objectName, this);
				SaveScene ();
				_element++;
			}

			EditorGUILayout.EndHorizontal ();
		}
	#endregion
}