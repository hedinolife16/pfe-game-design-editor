using System;
using UnityEditor;
using UnityEngine;
using Newtonsoft.Json;
using System.Collections.Generic;

[JsonObject(IsReference = true)]
[Serializable]
public class ConnectorNode : Scene 
{
	#region Parameters
		public Conditions[] conditions = new Conditions[200];
		public ConnectorNode(int id, Vector2 position, string title, float width, float height, GUIStyle inPointStyle, GUIStyle outPointStyle, Action<ConnectionPoint> OnClickInPoint, Action<ConnectionPoint> OnClickOutPoint, Action<Scene> OnClickRemoveNode) : base(id, position, title, width, height, inPointStyle,  outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveNode)
		{
			//Connector Contructor
			id = -1;
		}
	#endregion

	#region GUI Methods

		public override void Drag(Vector2 delta)
		{
			base.Drag(delta);
		}

		public override void DrawBox()
		{
			base.DrawBox();
		}

		public override void DrawHeader()
		{
			base.DrawHeader();
		}

		public override void DrawContent() 
		{
			base.DrawContent();

			GUILayout.BeginArea(rect);
				GUILayout.Space(30);
				
				RecieveConnectionCount();

				Conditions();

			GUILayout.EndArea();
		}

		private void RecieveConnectionCount()
		{
			GUILayout.FlexibleSpace();
			EditorGUILayout.BeginHorizontal();
				if (recieveConnection != null)
					if (recieveConnection.sceneName == "")
						EditorGUILayout.LabelField(recieveConnection.sceneId + " has : " + connectedWith.Count + " connections");
					else
						EditorGUILayout.LabelField(recieveConnection.sceneName + " has : " + connectedWith.Count + " connections");
			EditorGUILayout.EndHorizontal();
			GUILayout.FlexibleSpace();
		}

		private void Conditions()
		{
			if (connectedWith.Count >= 1 && recieveConnection != null)
			{
				ListingConditions();
			}
		}
	#endregion

	#region Helpers
		private void ListingConditions()
		{
			foreach (Scene nextScene in connectedWith)
			{
				GUILayout.FlexibleSpace();
				EditorGUILayout.BeginHorizontal();

					NewCondition(nextScene);

				EditorGUILayout.EndHorizontal();
				GUILayout.FlexibleSpace();
			}
		}
		private void NewCondition(Scene nextScene)
		{
			if (nextScene.sceneName == "")
			{
				GUILayout.Label(" Case Condition ");
				conditions[nextScene.sceneId] = (Conditions)EditorGUILayout.EnumPopup(conditions[nextScene.sceneId]);
				GUILayout.Label(" Go to  :  " + nextScene.sceneId);
				TriggerCondition(conditions[nextScene.sceneId]);
			}
			else
			{
				GUILayout.Label(" Case Condition ");
				conditions[nextScene.sceneId] = (Conditions)EditorGUILayout.EnumPopup(conditions[nextScene.sceneId]);
				GUILayout.Label(" Go to  :  " + nextScene.sceneName);
			}
		}

		void TriggerCondition(Conditions c) 
		{
			
		}
	#endregion
}