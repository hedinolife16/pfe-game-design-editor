using System;
using UnityEditor;
using UnityEngine;
using Newtonsoft.Json;
using System.Collections.Generic;

[JsonObject(IsReference = true)]
[Serializable]
public class Connection
{
	#region Parameters
		public ConnectionPoint to = null;
		public ConnectionPoint from = null;
		public Action<Connection> OnClickRemoveConnection = null;

		public Connection (ConnectionPoint to, ConnectionPoint from, Action<Connection> OnClickRemoveConnection)
		{
			this.to = to;
			this.from = from;
			this.OnClickRemoveConnection = OnClickRemoveConnection;
		}
		public Connection(){}
	#endregion

	#region GUI Methods
		public void Draw ()
		{
			Handles.DrawBezier (
				to.rect.center,
				from.rect.center,
				to.rect.center + Vector2.left * 50f,
				from.rect.center - Vector2.left * 50f,
				Color.green,
				null,
				5f
			);
	
			Handles.Button ((to.rect.center + from.rect.center) * 0.5f, Quaternion.identity, 4, 8, Handles.RectangleHandleCap);
		}
	#endregion
}