using System;
using UnityEditor;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using System.Collections.Generic;

public class DesignMaker : EditorWindow
{
	#region Parameters
		bool disable = false;
		
		private string _story;
		private string _playerName;
		private string _playerStory;
		private string[] _npcName = new string[30];
		private string[] _npcStory = new string[30];
		private int _npcCount = 1;
		public string[] characters = new string[31];
		public static void OpenWindow()
		{
			DesignMaker window = GetWindow<DesignMaker> ();
			window.titleContent = new GUIContent ("Sandy Editor");
			window.position = new Rect(15, 15 , 1000, 1000);
		}
	#endregion

	#region GUI Methods
		private void OnGUI() 
		{
			GUILayout.Label("Maker");

			GameName();
			ApplyAndGoToLevelEditor();
		}

		private void GameName()
		{
			//string json = GetDataPath();
			//SandyData data = JsonConvert.DeserializeObject<SandyData>(json);
			if(Template.gameName == true)
			{
				GUILayout.FlexibleSpace();
				EditorGUILayout.BeginHorizontal();
					EditorGUI.BeginDisabledGroup (disable);
						GUILayout.Label("Project Name : ");
						var name = EditorGUILayout.TextField("");
					EditorGUI.EndDisabledGroup ();
					if (GUILayout.Button ("Set", GUILayout.Height (15)))
					{
						disable = true;
					}
				EditorGUILayout.EndHorizontal();
				GUILayout.FlexibleSpace();
			}
		}

		private void StorylineAndCharacters()
		{
			GUILayout.FlexibleSpace();
			EditorGUILayout.BeginHorizontal();
				GUILayout.Label("Describe Storyline basis : ");
				_story = EditorGUILayout.TextArea(_story);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
				GUILayout.Label("CHARACTERS");
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
				GUILayout.Label("Player : ");
				_playerName = EditorGUILayout.TextArea(_playerName, GUILayout.Height(20));
				_playerStory = EditorGUILayout.TextArea(_playerStory, GUILayout.Height(20));
				if (GUILayout.Button ("Set Player", GUILayout.Height (15)))
					characters[0] = _playerName;
				
				if (GUILayout.Button ("Add New NPC", GUILayout.Height (15)))
				{
					if(_npcCount <= 31)
						AddNPC(_npcCount);
					else
						Debug.LogError("Cannot create more than 30 different NPC's");

					Debug.Log(characters[_npcCount]);
					_npcCount++;
				}
					
			EditorGUILayout.EndHorizontal();
			GUILayout.FlexibleSpace();
		}

		private void ApplyAndGoToLevelEditor()
		{
			if (GUILayout.Button ("Apply", GUILayout.Height (15)))
			{
				this.Close();
				LevelEditor.OpenWindow();
			}
		}
	#endregion

	#region Helpers
		private static string GetDataPath()
		{
			OperatingSystem os = Environment.OSVersion;
			PlatformID pid = os.Platform;

			switch(pid)
			{
				case PlatformID.Win32NT:
				case PlatformID.Win32S:
				case PlatformID.Win32Windows:
				case PlatformID.WinCE:
					return Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\sandy.json";
				case PlatformID.Unix:
					return null;
				case PlatformID.MacOSX:
					return Environment.GetFolderPath(Environment.SpecialFolder.Personal) + @"\sandy.json";
				default:
					return null;
			}
		}

		private void AddNPC(int i)
		{
			GUILayout.Label("NPC " + i + " : ");
			_npcName[i] = EditorGUILayout.TextArea(_npcName[i], GUILayout.Height(20));
			_npcStory[i] = EditorGUILayout.TextArea(_npcStory[i], GUILayout.Height(20));
			if (GUILayout.Button ("Add NPC", GUILayout.Height (15)))
				characters[i] = _npcName[i];
		}

	#endregion
}