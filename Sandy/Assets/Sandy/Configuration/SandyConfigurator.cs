using System;
using UnityEditor;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace Sandy
{
	[InitializeOnLoad]
	public class SandyConfigurator : EditorWindow
	{
		public static int currentState = 1;
		public static Data dataLoaded = null;

		public static bool showHelp;

		[MenuItem ("Window/Sandy Configurator")]
		public static void OpenWindow()
		{
			SandyConfigurator window = GetWindow<SandyConfigurator> ();
			window.titleContent = new GUIContent ("Sandy Configurator");
			window.position = new Rect(15, 15 , 300, 300);
		}

		public static void Quit()
		{
			SandyData(currentState, Json.Write, null);
		}

		public static void SandyData(int level, Json type, Data writeData)
		{
			JsonConvert.DefaultSettings = () => new JsonSerializerSettings
			{
				PreserveReferencesHandling = PreserveReferencesHandling.Objects,
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
				Formatting = Newtonsoft.Json.Formatting.Indented,
				NullValueHandling = NullValueHandling.Include,
				ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
				DefaultValueHandling = DefaultValueHandling.Populate
			};
			
			if (type == Json.Write)
			{
				string json;
				if (level == 1)
				{
					json = JsonConvert.SerializeObject(writeData);
				}
				else
				{
					if (level == 2)
					{
						json = JsonConvert.SerializeObject(writeData);
					}
					else
					{
						json = JsonConvert.SerializeObject(writeData);
					}
				}

				OperatingSystem os = Environment.OSVersion;
				PlatformID pid = os.Platform;

				switch (pid) 
				{
					case PlatformID.Win32NT:
					case PlatformID.Win32S:
					case PlatformID.Win32Windows:
					case PlatformID.WinCE:
						string pathWin = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\sandy.json";
						if (File.Exists(pathWin))
						{
							File.Delete(pathWin);
							Debug.Log("Windows detected! saved on :" + pathWin);
							File.WriteAllText(pathWin, json);
						}
						else
						{
							Debug.Log("Windows detected! saved on :" + pathWin);
							File.WriteAllText(pathWin, json);
						}
						break;
					case PlatformID.Unix:
						Debug.Log("Linux not supported yet");
						break;
					case PlatformID.MacOSX:
						string pathOSX = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + @"\sandy.json";
						if (File.Exists(pathOSX))
						{
							File.Delete(pathOSX);
							Debug.Log("Windows detected! saved on :" + pathOSX);
							File.WriteAllText(pathOSX, json);
						}
						else
						{
							Debug.Log("Windows detected! saved on :" + pathOSX);
							File.WriteAllText(pathOSX, json);
						}
						break;
					default:
						Debug.Log("No Idea what I'm on!");
						break;
				}
			}

			if (type == Json.Read)
			{
				OperatingSystem os = Environment.OSVersion;
				PlatformID pid = os.Platform;

				switch (pid) 
				{
					case PlatformID.Win32NT:
					case PlatformID.Win32S:
					case PlatformID.Win32Windows:
					case PlatformID.WinCE:
						string pathWin = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\sandy.json";
						if (!File.Exists(pathWin))
						{
							Debug.LogError("Something went wrong !");
						}
						else
						{
							Debug.Log("Windows detected! loaded from :" + pathWin);
							string json = pathWin;
							dataLoaded = JsonConvert.DeserializeObject<Data>(json);
						}
						break;
					case PlatformID.Unix:
						Debug.Log("Linux not supported yet");
						break;
					case PlatformID.MacOSX:
						string pathOSX = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + @"\sandy.json";
						if (!File.Exists(pathOSX))
						{
							Debug.LogError("Something went wrong !");
						}
						else
						{
							Debug.Log("Windows detected! loaded from :" + pathOSX);
							string json = pathOSX;
							dataLoaded = JsonConvert.DeserializeObject<Data>(json);
						}
						break;
					default:
						Debug.Log("No Idea what I'm on!");
						break;
				}
			}
		}

		public static void JsonLevelEditor(Json type, LevelEditor writeData)
		{
			if (type == Json.Write)
			{
				var json = JsonUtility.ToJson(writeData, true);

				OperatingSystem os = Environment.OSVersion;
				PlatformID pid = os.Platform;

				switch (pid) 
				{
					case PlatformID.Win32NT:
					case PlatformID.Win32S:
					case PlatformID.Win32Windows:
					case PlatformID.WinCE:
						string pathWin = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\sandy editor.json";
						if (File.Exists(pathWin))
						{
							File.Delete(pathWin);
							Debug.Log("Windows detected! saved on :" + pathWin);
							File.WriteAllText(pathWin, json);
						}
						else
						{
							Debug.Log("Windows detected! saved on :" + pathWin);
							File.WriteAllText(pathWin, json);
						}
						break;
					case PlatformID.Unix:
						Debug.Log("Linux not supported yet");
						break;
					case PlatformID.MacOSX:
						string pathOSX = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + @"\sandy editor.json";
						if (File.Exists(pathOSX))
						{
							File.Delete(pathOSX);
							Debug.Log("Windows detected! saved on :" + pathOSX);
							File.WriteAllText(pathOSX, json);
						}
						else
						{
							Debug.Log("Windows detected! saved on :" + pathOSX);
							File.WriteAllText(pathOSX, json);
						}
						break;
					default:
						Debug.Log("No Idea what I'm on!");
						break;
				}
				
				EditorPrefs.SetString("Level Editor", json);
			}

			if (type == Json.Read)
			{
				OperatingSystem os = Environment.OSVersion;
				PlatformID pid = os.Platform;

				switch (pid) 
				{
					case PlatformID.Win32NT:
					case PlatformID.Win32S:
					case PlatformID.Win32Windows:
					case PlatformID.WinCE:
						string pathWin = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\sandy editor.json";
						if (!File.Exists(pathWin))
						{
							Debug.LogError("Something went wrong !");
						}
						else
						{
							Debug.Log("Windows detected! loaded from :" + pathWin);
							var data = EditorPrefs.GetString("Level Editor", JsonUtility.ToJson(writeData, false));
							JsonUtility.FromJsonOverwrite(data, writeData);
						}
						break;
					case PlatformID.Unix:
						Debug.Log("Linux not supported yet");
						break;
					case PlatformID.MacOSX:
						string pathOSX = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + @"\sandy editor.json";
						if (!File.Exists(pathOSX))
						{
							Debug.LogError("Something went wrong !");
						}
						else
						{
							Debug.Log("Windows detected! loaded from :" + pathOSX);
							string json = pathOSX;
							var data = EditorPrefs.GetString("Level Editor", JsonUtility.ToJson(writeData, false));
							JsonUtility.FromJsonOverwrite(data, writeData);
						}
						break;
					default:
						Debug.Log("No Idea what I'm on!");
						break;
				}
			}
		}
		public static void ShowHelp(string message, int type)
		{
			if (showHelp)
			{
				switch(type)
				{
					case 1 :
						EditorGUILayout.HelpBox (message, MessageType.Info);
						break;
					case 2 :
						EditorGUILayout.HelpBox (message, MessageType.Warning);
						break;
					case 3 :
						EditorGUILayout.HelpBox (message, MessageType.Error);
						break;
				}
			}
		}

	}
	
	[Serializable]
	public class Data 
	{
		#region Template Data
			[JsonProperty(PropertyName = "Template Data")]
			public virtual bool gameName { set; get; }
			public virtual bool gameAnalysis { set; get; }
			public virtual bool genreAndThemes { set; get; }
			public virtual bool plateForms { set; get; }
			public virtual bool targetAudiance { set; get; }
			public virtual bool storylineAndChatacters { set; get; }
			public virtual bool gameplay { set; get; }
			public virtual bool gameAesthetics { set; get; }
		#endregion

		#region Maker Data

		#endregion

		#region Level Editor Data
			public virtual int instanceCreated { set; get; }
			public virtual List<Scene> scenes { set; get; }
			public virtual List<Connection> connections { set; get; }
		#endregion

		#region Miscellaneous Data
			public virtual DateTime dateTime { set; get; }
		#endregion
	}
}

public enum Json
{
	Read = 0, Write = 1
}
