using System;
using UnityEngine;
using UnityEditor;
using Sandy;

public class Template : EditorWindow
{
	#region Parameters
		public static Template window;
		public static bool 	gameName = true,
							gameAnalysis,
							genreAndThemes,
							plateForms,
							targetAudiance,
							storylineAndChatacters,
							gameplay = true,
							gameAesthetics;

		[MenuItem ("Window/Sandy Editor")]
		public static void OpenWindow()
		{
			window = GetWindow<Template> ();
			window.titleContent = new GUIContent ("Sandy Editor");
			window.position = new Rect(15, 15 , 1000, 1000);
		}
	#endregion
	
	#region GUI Methods
		public void OnGUI()
		{
			TestCurrentState(); //Testing if the current state is Template, else go to Maker or LevelEditor

			//All infos we need
			GameName();
			GameAnalysis();
			GenreAndThemes();
			PlateForms();
			TargetAudiance();
			StorylineAndChatacters();
			Gameplay();
			GameAesthetics();

			GUILayout.FlexibleSpace();

			ApplyAndGoToMaker();
			PassTemplate();
			Repaint();
		}

		private void TestCurrentState()
		{
			if (SandyConfigurator.currentState == 2)
			{
				System.Threading.Thread.Sleep(100);
				this.Close();
				DesignMaker.OpenWindow();
			}
			else
			{
				if (SandyConfigurator.currentState == 3)
				{
					System.Threading.Thread.Sleep(100);
					this.Close();
					LevelEditor.OpenWindow();
				}
			}
		}
		protected void GameName()
		{
			GUILayout.FlexibleSpace();
			EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();

				EditorGUI.BeginDisabledGroup (true);
				Template.gameName = EditorGUILayout.Toggle("Game Name", gameName);
				EditorGUI.EndDisabledGroup ();

				GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();
				EditorGUILayout.HelpBox ("Warning ! By clicking on edit.\nYou no longer can re-name your scene", MessageType.None);
				GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

			GUILayout.FlexibleSpace();
		}

		protected void GameAnalysis()
		{
			GUILayout.FlexibleSpace();
			EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();

				//EditorGUI.BeginDisabledGroup (true);
				Template.gameAnalysis = EditorGUILayout.Toggle("Game Analysis", gameAnalysis);
				//EditorGUI.EndDisabledGroup ();

				GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();
				EditorGUILayout.HelpBox ("Warning ! By clicking on edit.\nYou no longer can re-name your scene", MessageType.None);
				GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

			GUILayout.FlexibleSpace();
		}

		protected void GenreAndThemes()
		{
			GUILayout.FlexibleSpace();
			EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();

				//EditorGUI.BeginDisabledGroup (true);
				Template.genreAndThemes = EditorGUILayout.Toggle("Genre And Themes", genreAndThemes);
				//EditorGUI.EndDisabledGroup ();

				GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();
				EditorGUILayout.HelpBox ("Warning ! By clicking on edit.\nYou no longer can re-name your scene", MessageType.None);
				GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

			GUILayout.FlexibleSpace();
		}

		protected void PlateForms()
		{
			GUILayout.FlexibleSpace();
			EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();

				//EditorGUI.BeginDisabledGroup (true);
				Template.plateForms = EditorGUILayout.Toggle("PlateForms", plateForms);
				//EditorGUI.EndDisabledGroup ();

				GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();
				EditorGUILayout.HelpBox ("Warning ! By clicking on edit.\nYou no longer can re-name your scene", MessageType.None);
				GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

			GUILayout.FlexibleSpace();
		}

		protected void TargetAudiance()
		{
			GUILayout.FlexibleSpace();
			EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();

				//EditorGUI.BeginDisabledGroup (true);
				Template.targetAudiance = EditorGUILayout.Toggle("Target Audiance", targetAudiance);
				//EditorGUI.EndDisabledGroup ();

				GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();
				EditorGUILayout.HelpBox ("Warning ! By clicking on edit.\nYou no longer can re-name your scene", MessageType.None);
				GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

			GUILayout.FlexibleSpace();
		}

		protected void StorylineAndChatacters()
		{
			GUILayout.FlexibleSpace();
			EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();

				//EditorGUI.BeginDisabledGroup (true);
				Template.storylineAndChatacters = EditorGUILayout.Toggle("Storyline And Chatacters", storylineAndChatacters);
				//EditorGUI.EndDisabledGroup ();

				GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();
				EditorGUILayout.HelpBox ("Warning ! By clicking on edit.\nYou no longer can re-name your scene", MessageType.None);
				GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

			GUILayout.FlexibleSpace();
		}

		protected void Gameplay()
		{
			GUILayout.FlexibleSpace();
			EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();

				EditorGUI.BeginDisabledGroup (true);
				Template.gameplay = EditorGUILayout.Toggle("Gameplay", gameplay);
				EditorGUI.EndDisabledGroup ();

				GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();
				EditorGUILayout.HelpBox ("Warning ! By clicking on edit.\nYou no longer can re-name your scene", MessageType.None);
				GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

			GUILayout.FlexibleSpace();
		}

		protected static void GameAesthetics()
		{
			GUILayout.FlexibleSpace();
			EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();

				//EditorGUI.BeginDisabledGroup (true);
				//var temp = false;
				Template.gameAesthetics = EditorGUILayout.Toggle("Game Aesthetics", Template.gameAesthetics);
				//Template.gameAesthetics = !Template.gameAesthetics;
				//Debug.Log(Template.gameAesthetics);
				//EditorGUI.EndDisabledGroup ();

				GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();
				EditorGUILayout.HelpBox ("Warning ! By clicking on edit.\nYou no longer can re-name your scene", MessageType.None);
				GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

			GUILayout.FlexibleSpace();
		}

		private void ApplyAndGoToMaker()
		{

			//TODO : FIX SECOND TIME APPLY CHANGE
			if (GUILayout.Button ("Apply", GUILayout.Height (15)))
			{
				this.Repaint();
				AssetDatabase.Refresh();

				Debug.Log(gameName);
				Debug.Log(gameAnalysis);
				Debug.Log(genreAndThemes);
				Debug.Log(plateForms);
				Debug.Log(storylineAndChatacters);
				Debug.Log(targetAudiance);
				Debug.Log(gameplay);
				Debug.Log(gameAesthetics);

				Data data = new Data()
				{
					gameName = Template.gameName,
					gameAnalysis = Template.gameAnalysis,
					genreAndThemes = Template.genreAndThemes,
					plateForms = Template.plateForms,
					targetAudiance = Template.targetAudiance,
					storylineAndChatacters = Template.storylineAndChatacters,
					gameplay = Template.gameplay,
					gameAesthetics = Template.gameAesthetics,
					instanceCreated = LevelEditor.instanceCreated,
					dateTime = DateTime.Now,
				};

				SandyConfigurator.SandyData(1, Json.Write, data);

				this.Close();
				SandyConfigurator.currentState = 2;
				DesignMaker.OpenWindow();
			}
		}

		private void PassTemplate()
		{
			if (GUILayout.Button ("Pass this part", GUILayout.Height (15)))
			{
				this.Repaint();
				AssetDatabase.Refresh();

				this.Close();
				DesignMaker.OpenWindow();
			}
		}
	#endregion
}